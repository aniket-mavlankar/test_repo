#Test repo sale.order.line

# -*- coding: utf-8 -*-

try:
    import simplejson as json
except ImportError:
    import json




BONUS_SELF_SUPPORTED = "Bonus supported by Brodynt"
PORCENTAJE_ANUAL_BONUS_BRUTO = 6.6 #5% de bonus, 1.6 mas (31.5%) por tema irpf, por tanto es el bonus en bruto
PORCENTAJE_ANUAL_BONUS_NETO = 5
PORCENTAJE_ANUAL_BONUS = PORCENTAJE_ANUAL_BONUS_BRUTO

MAX_AMORTIZACION_BONUS = 3
MIN_SIMILITUD = 90

#Available States.
AVAILABLE_STATES = [
    ('cancel', 'Cancelled'),
    ('draft', 'Draft'),
    ('to_review', 'To Be Reviewed'),
    ('to_approve', 'To Approve'),
    ('to_purchase', 'To Purchase'),
    ('spofing', 'SPOFing'),
    ('confirmed', 'Signed'),
    ('exception', 'Rollback'),
    ('provisioning', 'Provisioning'),
    ('done', 'Done'),
    ('to_cancel', 'To Be Cancelled'),
    ('new', 'New State added for testing')
]

#Help text for State
STATES_HELP = '* The \'Draft\' status is set when the related sales order in draft or sent status. \
            \n* The \'To Be Reviewed\' status is set when the line is pending for review from the account manager. \
            \n* The \'To Approve\' status is set when the line is pending for sale approval from the operations managers. \
            \n* The \'To Purchase\' status is set when the line is pending for purchase. \
            \n* The \'SPOFing\' status is set when the line is pending for purchase approval from the operations managers. \
            \n* The \'Signed\' status is set when the related sales order is confirmed by the customer. \
            \n* The \'Provisioning\' status is set when the sales order line is at provisioning stage. \
            \n* The \'Done\' status is set when the sales order line has been installed. \
            \n* The \'Rollback\' status is set when the line is set as a rollback. \
            \n* The \'To Be Cancelled\' status is set when the customer has requested to cancel the line. \
            \n* The \'Cancelled\' status is set when the line is cancelled.'

# Available States Portal.
AVAILABLE_STATES_PORTAL = [
    ('open', 'Open'),
    ('paperwork', 'Paperwork'),
    ('provisioning', 'In provisioning'),
    ('done', 'Installed'),
    ('cancel', 'Cancelled'),
    ('matrix', 'Matrix')
]

# States Mapping between normal states and Portal states.
STATES_MAPPING = {
    'draft': 'open',
    'to_review': 'paperwork',
    'to_approve': 'paperwork',
    'to_purchase': 'paperwork',
    'spofing': 'paperwork',
    'confirmed': 'paperwork',
    'exception': 'paperwork',
    'provisioning': 'provisioning',
    'cancel': 'cancel',
    'done': 'done',
    'to_cancel': 'paperwork'
}

#Available Location Types.
AVAILABLE_LOCATION_TYPES = [
    ('airport', 'Airport'),
    ('mall', 'Mall'),
    ('datacenter', 'Datacenter'),
    ('windfarm', 'Windfarm'),
    ('others', 'Others'),
    ('broynt', 'Brodynt Office')
]

LOCATION_AIRPORT = AVAILABLE_LOCATION_TYPES[0][0]
LOCATION_MALL = AVAILABLE_LOCATION_TYPES[1][0]
LOCATION_DATACENTER = AVAILABLE_LOCATION_TYPES[2][0]

#Available Speed Settings.
AVAILABLE_SPEED_SETTINGS = [
    ('auto', 'AUTO'),
    ('10', '10'),
    ('100', '100'),
    ('1000', '1000')
]

# Available Duplex Settings.
AVAILABLE_DUPLEX_SETTINGS = [
    ('auto', 'AUTO'),
    ('half', 'HALF'),
    ('duplex', 'DUPLEX'),
    ('noduplex', 'NO')
]

AVAILABLE_MESSAGE_TYPES = [
    ('email,comment,notification','All'),
    ('email', 'Email'),
    ('comment', 'Comment'),
    ('notification', 'System notification'),
]

#IP Ranges as DICT.
IP_RANGES_DICT = {i[0]: i[1] for i in AVAILABLE_IP_RANGES}


#FULL_ADDRESS_FIELDS = ['requested_country', 'area', 'city', 'requested_cp', 'requested_address']

THRUK_TEMPLATE = "http://51.254.218.177/brodynt/thruk/#cgi-bin/extinfo.cgi?type=1&host={0}" #&backend=ff28f#pnp_th2/1505995174/1506085174"
ADAGIOS_TEMPLATE = "http://51.254.120.13/adagios/status/detail?host_name={0}"

def test_function(a, b):
    return a + b

print(test_function(1, 2))

def testings():
    assertEqual(test_function(1, 1), 2)

def issue_test_function():
    pass

def function_demo():
    print "hola"

def function_pull():
    sadfg